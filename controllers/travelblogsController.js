import TravelBlogs from '../models/travelBlogsModel.js';


/**
 * @desc		Get all travelblogs
 * @router	GET /api/travelblogs
 * @access	public
 */
const getTravelBlogs = async (req, res) => {
	const travelblogs = await TravelBlogs.find({});
	res.json(travelblogs);
};

/**
 * @desc		Get single products
 * @router	GET /api/products/:id
 * @access	public
 */
const getTravelBlogId = async (req, res) => {
	const travelid = await TravelBlogs.findById(req.params.id);

	if (travelid) {
		res.json(travelid);
	} else {
		res.status(404);
		throw new Error('TravelBlog not found');
	}
};

/**
 * @desc		Delete a travelblog
 * @router	DELETE /api/travelblogs/:id
 * @access	public/user
 */
const deletetravelblog = async (req, res) => {
	const travelblog = await TravelBlogs.findById(req.params.id);

	if (travelblog) {
		await TravelBlogs.deleteOne(travelblog);
		res.json({ message: 'Blog deleted' });
	} else {
		res.status(404);
		throw new Error('Blog not found');
	}
};

const createTravelBlog = async (req, res) => {
	const travelblog = new TravelBlogs({
		user: req.user._id,
		placename: 'City Name',
		image: '/images/sample.jpg',
		googleLink: 'https://maps.app.goo.gl/M812345678ABC',
		placedes: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime, magni! Earum sunt quis voluptatem provident consequatur totam odio sint architecto repellendus.',
		nearbyplaces: [],
	});
	
	const createdtravelblog = await travelblog.save();
	res.status(201).json(createdtravelblog);



};

const updateTravelBlog = async (req, res) => {
	const { placename, image, googlelink,placedes, nearbyplaces } =
		req.body;

	const travelblog = await TravelBlogs.findById(req.params.id);

	if (travelblog) {
		travelblog.placename = placename;
		travelblog.image = image;
		travelblog.googlelink = googlelink;
		travelblog.placedes = placedes;
		travelblog.nearbyplaces = nearbyplaces;

		const updatedTravelBlog = await travelblog.save();
		res.json(updatedTravelBlog);
	} else {
		res.status(404);
		throw new Error('Blog not found');
	}
};


export {getTravelBlogs, getTravelBlogId, deletetravelblog, createTravelBlog, updateTravelBlog};