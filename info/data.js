const data = [{

    placename: 'Pune',
    image: '/images/Pune.jpg',
    Images: ['/images/Pune.jpg', '/images/Dagdusheth.jpg', '/images/Pune-Okayama.jpg', '/images/Sinhagad-Fort.webp'],
    googlelink: 'https://maps.app.goo.gl/M84fJPWd2WXmiYMW7',
    placedes: 'Pune is a sprawling city in the western Indian state of Maharashtra. It was once the base of the Peshwas (prime ministers) of the Maratha Empire, which lasted from 1674 to 1818. Its known for the grand Aga Khan Palace, built in 1892 and now a memorial to Mahatma Gandhi, whose ashes are preserved in the garden.',
    nearbyplaces: [`Pawna Lake,Lonavala,Lavasa`],
    author:'John Doe',
    hotels: [{
        nearbyname: "Ibis Pune Viman Nagar",
        rating: "7.9",
        price: "8000",
        neargooglelink: "https://maps.app.goo.gl/RyaPturTveBNf5hu8",
    }, {
        nearbyname: "O Hotel Pune",
        rating: "7.5",
        price: "6800",
        neargooglelink: "https://maps.app.goo.gl/cPgyaFt85UmneWjN6",

    }]

}, {

    placename: 'Mumbai',
    image: '/images/mumbai.webp',
    Images: ['/images/mumbai.webp', '/images/SiddhivinayakTemple.jpg', '/images/Pagoda.jpg', '/images/GOI.webp'],
    googlelink: 'https://maps.app.goo.gl/CixVfe97mq2K1Bof9',
    placedes: 'Mumbai (formerly called Bombay) is a densely populated city on India’s west coast. A financial center, its Indias largest city. On the Mumbai Harbour waterfront stands the iconic Gateway of India stone arch, built by the British Raj in 1924. Offshore, nearby Elephanta Island holds ancient cave temples dedicated to the Hindu god Shiva.',
    nearbyplaces: [`Rajmachi,ajanta & ellora caves,Shirdi`],
    author:'Jane Doe',
    hotels: [{
        nearbyname: "Novotel Mumbai Juhu Beach",
        rating: "8.1",
        price: "10000",
        neargooglelink: "https://maps.app.goo.gl/yALMFKP7z1bsrgBm7",
    }, {
        nearbyname: "Hotel Deluxe Residency",
        rating: "7.2",
        price: "2600",
        neargooglelink: "https://maps.app.goo.gl/ZGKXjrPMFbfrivda9",

    }]

}, {

    placename: 'Lonavala',
    image: '/images/Lonavala.avif',
    googlelink: 'https://maps.app.goo.gl/SPi1PyAMGJdgYjMV8',
    Images: ['/images/Lonavala.avif', '/images/Karla.jpg', '/images/KuneFalls.jpg', '/images/dellapark.png'],
    placedes: 'Lonavala is a hill station surrounded by green valleys in western India near Mumbai. The Karla Caves and the Bhaja Caves are ancient Buddhist shrines carved out of the rock. They feature massive pillars and intricate relief sculptures. South of the Bhaja Caves sits the imposing Lohagad Fort, with its 4 gates.',
    nearbyplaces: [`Imagica,Lonavala lake,Karla caves`],
    author:'John Doe',
    hotels: [{
        nearbyname: "Radisson Resort and Spa, Lonavala",
        rating: "8.2",
        price: "12,900",
        neargooglelink: "https://maps.app.goo.gl/d4ykrfYoKKWzTbFb6",
    }, {
        nearbyname: "Hotel Sai Niwas",
        rating: "6.2",
        price: "1100",
        neargooglelink: "https://maps.app.goo.gl/DkCgS6Hf9pCgp6NZ7",

    }]

}, {

    placename: 'Karjat',
    image: '/images/Karjat.jpg',
    Images: ['/images/Karjat.jpg', '/images/Kothaligad.jpg', '/images/Karnala.jpg', '/images/Bhivpuri.jpg'],
    googlelink: 'https://maps.app.goo.gl/Nyb3VmnzjmGTZpLw9',
    placedes: 'Karjat is a city near Mumbai, in the Indian state of Maharashtra. Its steep Ulhas Valley is lush in the rainy season, with a full river and waterfalls. Bahiri Cave, overlooking the valley, is a Hindu pilgrimage site. The ancient Kondeshwar Temple is dedicated to Lord Shiva. The Kondana Caves are 16 man-made caverns dating from the 1st century B.C., with stupas and statues.',
    nearbyplaces: [`Kondana caves,Peth fort,Bhivpuri waterfalls`],
    author:'Jane Doe',
    hotels: [{
        nearbyname: "Radisson Blu Plaza Resort & Convention Center",
        rating: "7.9",
        price: "12,000",
        neargooglelink: "https://maps.app.goo.gl/oWzt3qAdU5Td9JFU9",
    }, {
        nearbyname: "Hotel Metro Palace Resort",
        rating: "4.0",
        price: "1700",
        neargooglelink: "https://maps.app.goo.gl/HeDDAwaNti33iSnG8",

    }]

}, {

    placename: 'Pali',
    image: '/images/Pali.jpg',
    Images: ['/images/Pali.jpg', '/images/Bangur.jpg', '/images/Lakhotia.avif', '/images/Ranakupur.avif'],
    googlelink: 'https://maps.app.goo.gl/MUqauWaHuZ31BQDj9',
    placedes: 'Pali is a city and capital division in Pali district in Indian state of Rajasthan. It is the administrative headquarters of Pali district. It is on the bank of the river Bandi and is 70 km south east of West Jodhpur. It is known as "The Industrial City".',
    nearbyplaces: [`Jawai dam,Adinath temple,Bangur Museum`],
    author:'John Doe',
    hotels: [{
        nearbyname: "Anantvan Ranthambore by Asapian Hotels",
        rating: "9.2",
        price: "30,000",
        neargooglelink: "https://maps.app.goo.gl/9hHTxkYMULJmAPfx6",
    }, {
        nearbyname: "Chitra Niwas Palighat",
        rating: "6.5",
        price: "2500",
        neargooglelink: "https://maps.app.goo.gl/FCiEDYZw4oKjMn466",

    }]

}, {

    placename: 'Mahabaleshwar',
    image: '/images/Mahabaleshwar.webp',
    Images: ['/images/Mahabaleshwar.webp', '/images/MahabaleshwarTemple.jpg', '/images/PratapgadFort.jpg', '/images/ElphistonPoint.jpg'],
    googlelink: 'https://maps.app.goo.gl/6t6csvAXBsXnidpNA',
    placedes: 'Mahabaleshwar is a hill station in Indias forested Western Ghats range, south of Mumbai. It features several elevated viewing points, such as Arthur’s Seat. West of here is centuries-old Pratapgad Fort, perched atop a mountain spur. East, Lingmala Waterfall tumbles off a sheer cliff. Colorful boats dot Venna Lake, while 5 rivers meet at Panch Ganga Temple to the north',
    nearbyplaces: [`Pratapgad fort,Mapro garden,Panch Ganga temple`],
    author:'Jane Doe',
    hotels: [{
        nearbyname: "Le Meridian Mahabaleshwar Resort & Spa",
        rating: "8.8",
        price: "14,500",
        neargooglelink: "https://maps.app.goo.gl/LuQr66ejS4nfaC4h7",
    }, {
        nearbyname: "Mayuri Cottage",
        rating: "7.8",
        price: "1500",
        neargooglelink: "https://maps.app.goo.gl/WkJgpbtDoS5tKHS99",

    }]

}, {

    placename: 'Matheran',
    image: '/images/Matheran1.jpg',
    Images: ['/images/Matheran1.jpg', '/images/LousiaPoint.jpg', '/images/OneTree.webp', '/images/PanoramaPoint.jpg'],
    googlelink: 'https://maps.app.goo.gl/hQUVj2B2HdAxkoaz7',
    placedes: 'Matheran is a hill station, near Mumbai, in the west Indian state of Maharashtra. It’s known for its mild climate and well-preserved colonial architecture. Motor vehicles are banned and many visitors arrive by narrow-gauge railway on the 1907 Neral–Matheran Toy Train. The Panorama Point lookout offers views across the mountains of the Western Ghats.',
    nearbyplaces: [`Dapoli,Nashik,Satara`],
    author:'John Doe',
    hotels: [{
        nearbyname: "River Estates luxury resort & spa",
        rating: "9.2",
        price: "9000",
        neargooglelink: "https://maps.app.goo.gl/SaQcHq3iJDuWoJv99",
    }, {
        nearbyname: "New Green Hill",
        rating: "5.2",
        price: "2500",
        neargooglelink: "https://maps.app.goo.gl/VGL7qF6qWM9m9JGNA",

    }]

}, {

    placename: 'Alibaug',
    image: '/images/ALIBAUG1.jpg',
    Images: ['/images/ALIBAUG1.jpg', '/images/KolabaFort.jpg', '/images/VarsoliBeach.avif', '/images/AwasBeach.jpg'],
    googlelink: 'https://maps.app.goo.gl/2fWoBNw76CqaBk8a8',
    placedes: 'Alibag, also known as Alibaug, is a coastal town, just south of Mumbai, in western India. It’s known for its beaches like Alibag Beach and Varsoli Beach. Just offshore, 17th-century Kolaba Fort has carvings of tigers and elephants, and temples dedicated to Hindu gods. To the south, Portuguese-built Korlai Fort dates from 1521 and includes a lighthouse.',
    nearbyplaces: [`Alibaug beach,Kolaba fort,Kanakeshwar forest`],
    author:'Jane Doe',
    hotels: [{
        nearbyname: "Radisson Blu Resort & Spa",
        rating: "8.5",
        price: "13,000",
        neargooglelink: "https://maps.app.goo.gl/2Ak4Au7p7DSB7z4MA",
    }, {
        nearbyname: "O'NEST Countryside",
        rating: "6.7",
        price: "2500",
        neargooglelink: "https://maps.app.goo.gl/7s9WJgPkvXunuYdL7",

    }]

}, {

    placename: 'Aamby Valley City',
    image: '/images/aamby.jpg',
    Images: ['/images/aamby.jpg', '/images/Butterfly.jpg', '/images/RaigadFort.jpg', '/images/VisapurFort.jpg'],
    googlelink: 'https://maps.app.goo.gl/nP4RLTLNefXbnYky8',
    placedes: 'Aamby Valley City is a township developed by the Sahara India Pariwar on the outskirts of Pune, India. The township is connected by road from Lonavala. The township has an airstrip that stopped functioning in late 2016. It has an average annual rainfall of 4,000 mm (160 in) from June to September.',
    nearbyplaces: [`Korigad fort,Sinhagad fort, tiger point`],
    author:'John Doe',
    hotels: [{
        nearbyname: "Della Resorts",
        rating: "7.4",
        price: "16,500",
        neargooglelink: "https://maps.app.goo.gl/LzHAiCvU3FxvvZxd8",
    }, {
        nearbyname: "`Hotel Nandanvan by Royal Stay",
        rating: "7.8",
        price: "1500",
        neargooglelink: "https://maps.app.goo.gl/CAHMKbvibBfShLTK9",

    }]

}, {

    placename: 'Nashik',
    image: '/images/anjaneri-hills-nashik.jpg',
    Images: ['/images/nashik.jpg', '/images/nashik3.webp', '/images/nashik2.jpg',],
    googlelink: 'https://maps.app.goo.gl/Uo3qHzNc8i2XHPVm6',
    placedes: 'Nashik is an ancient city in Maharashtra, India, located on the banks of the Godavari River. Its about 165 km northeast of Mumbai and 210 km north of Pune, in the "Golden Triangle of Maharashtra" region. Nashik is known for its temples, wine, and religious significance',
    nearbyplaces: [`Sula vineyard,Panchvati,Trimbakeshwar Temple`],
    author:'Jane Doe',
    hotels: [{
        nearbyname: "Express Inn The Business Luxury Hotel",
        rating: "7.8",
        price: "8500",
        neargooglelink: "https://maps.app.goo.gl/kcQxdzhNqyhrJRJw8",
    }, {
        nearbyname: "Cool Palace Hotel",
        rating: "6.0",
        price: "1200",
        neargooglelink: "https://maps.app.goo.gl/f24iF8ZWaogWH7YD8",

    }]

}, {

    placename: 'Nagpur',
    image: '/images/nagpur1.jpg',
    Images: ['/images/nagpur4.jpg', '/images/nagpur3.jpg', '/images/nagpur2.cms'],
    googlelink: 'https://maps.app.goo.gl/zfS8jKY9UNooUaXU7',
    placedes: 'It is called the heart of India because of its central geographical location. It is the largest and most populated city in central India. Also known as the "Orange City", Nagpur is the 13th largest city in India by population.',
    nearbyplaces: [`Ramtek Fort Temple,Lata Mangeshkar musical garden, Amba Khori`],
    author:'John Doe',
    hotels: [{
        nearbyname: "Radisson Blu Hotel",
        rating: "8.2",
        price: "10,000",
        neargooglelink: "https://maps.app.goo.gl/aS8yx6nbfLSmC2ut6",
    }, {
        nearbyname: "Airport Hotel Relax Inn",
        rating: "8.0",
        price: "2500",
        neargooglelink: "https://maps.app.goo.gl/c6bKTJv91YQi68FbA",

    }]

}, {

    placename: 'Ahemdabad',
    image: '/images/Ahmedabad.avif',
    Images: ['/images/Ahmedabad.avif', '/images/SabarmatiAshram.jpg', '/images/SabarmatiRiverFront.jpg'],
    googlelink: 'https://maps.app.goo.gl/E21592zxbNTbMrF59',
    placedes: 'Ahmedabad, in western India, is the largest city in the state of Gujarat. The Sabarmati River runs through its center. On the western bank is the Gandhi Ashram at Sabarmati, which displays the spiritual leader’s living quarters and artifacts. Across the river, the Calico Museum of Textiles, once a cloth merchant’s mansion, has a significant collection of antique and modern fabrics.',
    nearbyplaces: [`Thol bird santuary,Kalpeshwar temple,Zanzari waterfalls`],
    author:'Jane Doe',
    hotels: [{
        nearbyname: "Taj skyline Ahmedabad",
        rating: "8.2",
        price: "10,000",
        neargooglelink: "https://maps.app.goo.gl/ueyRdBdMsRny5iYdA",
    }, {
        nearbyname: "Hotel Ozone Ahmeddabad",
        rating: "5.9",
        price: "1400",
        neargooglelink: "https://maps.app.goo.gl/9fd3FCQD4eCdkF3BA",

    }]

}, {

    placename: 'Solapur',
    image: '/images/science-museum-at-solapur.jpg',
    Images: ['/images/solapur1.jpg', '/images/solapur2.jpeg', '/images/solapur3.webp'],
    googlelink: 'https://maps.app.goo.gl/wcyWvGpVnbPDqMub8',
    placedes: 'Solapur, city, southern Maharashtra state, western India. It is situated in an upland region on the Sina River. In early centuries the city belonged to the Hindu Chalukyas and Devagiri Yadavas but later became part of the Muslim Bahmani and Bijapur kingdoms. Aheri is among two Scheduled Area listed under Gadhiroli district by Government Of India. Viththal Rukhmai Mandir is the famous temple here, located right at the heart of the town.',
    nearbyplaces: [`Solapur Bhuikot Fort, Rukmani temple,Tillus Agro Tourism`],
    author:'John Doe',
    hotels: [{
        nearbyname: "Balaji Sarover Premiere",
        rating: "7.9",
        price: "6500",
        neargooglelink: "https://maps.app.goo.gl/jdp8Y3R9o5e5Rzdw7",
    }, {
        nearbyname: "Hotel Kapish International",
        rating: "6.3",
        price: "1800",
        neargooglelink: "https://maps.app.goo.gl/uuivZqGCwDqhsEhQ7",

    }]

}, {

    placename: 'Nanded',
    image: '/images/visava-garden-vishnu-nagar-nanded-amusement-parks-wtifxm0szg.avif',
    Images: ['/images/nanded1.jpg', '/images/nanded2.jpg', '/images/nanded3.jpg'],
    googlelink: 'https://maps.app.goo.gl/8jfgWak5Gt2qdT6w6',
    placedes: 'Nanded is one of the historical places in Marathwada region of Maharashtra State. It is situated on north bank of Godavari river. It is famous for Sikh Gurudwaras. In 1708, the year following death of Aurangzeb, Guru Gobind Singh the tenth spiritual leader of the Sikhs came over to Nanded, his permanent abode.',
    nearbyplaces: [`Takhat Sachkhand Sri Hazur Sahib,Nanded Fort, Kandhar Fort`],
    author:'Jane Doe',
    hotels: [{
        nearbyname: "The Arowana Divine",
        rating: "8.8",
        price: "6000",
        neargooglelink: "https://maps.app.goo.gl/9KwF5hcni7jR9uN67",
    }, {
        nearbyname: "Hotel Bhaiji Executive",
        rating: "9.0",
        price: "2000",
        neargooglelink: "https://maps.app.goo.gl/EMiKPCTpoo6mcnhBA",

    }]

},{

    placename: 'Ahmednagar',
    image:'/images/Ahmednagar.jpg',
    Images:['/images/Ahmednagar.jpg','/images/AhmednagarFort.jpg','/images/CavalryTankMuesum.jpg','/images/VishalGanpatiTemple.jpg'],
    googlelink:'https://maps.app.goo.gl/DKA8cL4gpVwFP3kd9',
    placedes:'Ahmednagar is a city in, and the headquarters of, the Ahmednagar district, Maharashtra, India, about 120 km northeast of Pune and 114 km from Aurangabad. Ahmednagar has several dozen buildings and sites from the Nizam Shahi period.',
    nearbyplaces:[`Ahmednagar fort,Cavalry tank museum,Sandhan valley`],
    author:'John Doe',
    hotels: [{
        nearbyname: "Enrise by Sayaji Ahmednagar",
        rating: "6.5",
        price: "4000",
        neargooglelink: "https://maps.app.goo.gl/AVN4gKJFA6Y6KNkdA",
    }, {
        nearbyname: "Hotel Suvarnam Residency",
        rating: "7.0",
        price: "2300",
        neargooglelink: "https://maps.app.goo.gl/ND8LSv64AfR2BF349",

    }]


    
},{

    placename: 'Satara',
    image: '/images/satara4.jpg',
    Images: ['/images/satara1.jpg', '/images/satara2.jpg', '/images/satara3.jpg'],
    googlelink: 'https://maps.app.goo.gl/Y9oWMqJTsy15x1J38',
    placedes: 'SATARA was the capital of Maratha Kingdom spreaded over 14 lacks square kilometer. This land has rich heritage. Several great warriors, kings,saints, and great personalities create their historical evidence in the history of Maharashtra.',
    nearbyplaces: [` Thoseghar Waterfall, Kaas Pathar - Valley of Flowers, Sajjangad`],
    author:'Jane Doe',
    hotels: [{
        nearbyname: "The Fern Residency",
        rating: "8.4",
        price: "6000",
        neargooglelink: "https://maps.app.goo.gl/e8K9M1VgLgV9141a6",
    }, {
        nearbyname: "Hotel Sarang Lodging and Boarding",
        rating: "7.0",
        price: "1500",
        neargooglelink: "https://maps.app.goo.gl/vNem8CuKftfRunnt6",

    }]

},{

    placename: 'Ajgaon',
    image:'/images/ajgaon.jpg',
    Images:['/images/ajgaon.jpg','/images/VithobaTemple.jpg','/images/KunkeshwarBeach.jpg','/images/YashwantGad.jpg'],
    googlelink:'https://maps.app.goo.gl/fXefEAzfay7MoU1T9',
    placedes:'Ajgaon is a village in the state of Maharashtra, India. It is located in the Sawantwadi taluk of Sindhudurg district in Maharashtra. Ajgaon lies about 13 km south of Vengurla, in Sindhudurg District. It is the last major settlement along the highway before the road enters North Goa District of Goa.',
    nearbyplaces:[`Vetoba mandir,Moti lake,Yashwant gad fort`],
    author:'John Doe',
    hotels: [{
        nearbyname: "Kay's By The Sea",
        rating: "6.9",
        price: "3000",
        neargooglelink: "https://maps.app.goo.gl/G6jnbkt1cWyXrPos7",
    }, {
        nearbyname: "Sulochana Resort",
        rating: "5.6",
        price: "2500",
        neargooglelink: "https://maps.app.goo.gl/a5y51WogmTvTVhde8",

    }]

    
},{

    placename: 'Gavase',
    image:'/images/Gavse.avif',
    Images:['/images/Gavse.avif','/images/RankalaLake.jpg','/images/TownHallMuesum.jpg'],
    googlelink:'https://maps.app.goo.gl/yQNaBVHpA6MVCD619',
    placedes:'Gavase is a village located in the Sindhudurg district of Maharashtra, India. This village is part of the Dodamarg taluka and is known for its natural beauty, which includes lush greenery, hills, and water bodies. The region’s landscape makes it an attractive spot for those seeking a peaceful and picturesque environment.',
    nearbyplaces:[`Kalammawadi wildlife,Rautwadi waterfall,Savdav waterfall`],
    author:'Jane Doe',
    hotels: [{
        nearbyname: "Chalobhills Farmstay Resort ",
        rating: "9.9",
        price: "3600",
        neargooglelink: "https://maps.app.goo.gl/4W12LrUvq5qi8VYAA",
    }, {
        nearbyname: "Vardhan Lodging",
        rating: "6.5",
        price: "1500",
        neargooglelink: "https://maps.app.goo.gl/Ft84hjm2D8rxkCtK6",

    }]

    
},{

    placename: 'Akkalkot',
    image:'/images/Akkalkot.jpg',
    Images:['/images/Akkalkot.jpg','/images/AkkalkotTemple.jpg','/images/OldTemple.jpg'],
    googlelink:'https://maps.app.goo.gl/7aRtCM1FuuheECXk8',
    placedes:'Akkalkot is a city and a municipal council in Solapur district in the Indian state of Maharashtra. It is situated 38 km southeast of Solapur and close to the border of Karnataka state.',
    nearbyplaces:[`Akkalkot palace,Shri Swami Samarth mandir,,Old lake`],
    author:'John Doe',
    hotels: [{
        nearbyname: "Shankar Residency, Akkalkot",
        rating: "8.0",
        price: "3200",
        neargooglelink: "https://maps.app.goo.gl/KzbPALUULe6mQ7hCA",
    }, {
        nearbyname: "Hotel Kalika Residency",
        rating: "7.0",
        price: "1500",
        neargooglelink: "https://maps.app.goo.gl/fVkKXyNFhwhQDtto8",

    }]

    
},{

    placename: 'Chiplun',
    image: '/images/chiplun.jpeg',
    Images: ['/images/chiplun.jpeg', '/images/SawatSadaWaterFall.jpg', '/images/VashishtiRiver.jpg', '/images/GuhagarBeach.jpg'],
    googlelink: 'https://maps.app.goo.gl/fg1Hn74mg8ddB5CC9',
    placedes: 'Chiplun is a city in Ratnagiri district in the state of Maharashtra, India. It is one of the financial and commercial Hubs of Ratnagiri district, and the headquarters of Chiplun taluka. It is about 250 km south of Mumbai and 90 km North of Ratnagiri in the Konkan region of Maharashtra, on the Mumbai–Goa highway.',
    nearbyplaces: [`Vashishti river,Sawatsada waterfalls,Lord Parshurama temple `],
    author:'Jane Doe',
    hotels: [{
        nearbyname: "Surve Natures Resort - Chiplun",
        rating: "6.7",
        price: "2500",
        neargooglelink: "https://maps.app.goo.gl/itseKzaQJAvkCkmV7",
    }, {
        nearbyname: "Red Roof Farmhouse",
        rating: "8.6",
        price: "1800",
        neargooglelink: "https://maps.app.goo.gl/6cUBs25kiczWp7AQ9",

    }]
}, {
    placename: 'Goa',
    image: '/images/goa1.avif',
    Images: ['/images/goa2.jpg', '/images/goa3.jpg', '/images/goa5.jpg', '/images/goa4.webp'],
    googlelink: 'https://maps.app.goo.gl/V4H4pH3RSeMAf87w5',
    placedes: 'Goa is a state in India on the southwestern coast, in the Konkan region. Its known for its beaches, nightlife, churches, and architecture, and is a popular destination for tourists.',
    nearbyplaces: [`Dudhsagar Falls,Panjim,Calangute `],
    author:'John Doe',
    hotels: [{
        nearbyname: "Grand Hyatt Goa",
        rating: "8.6",
        price: "12,500",
        neargooglelink: "https://maps.app.goo.gl/VEpPujJwhwMUF9pYA",
    }, {
        nearbyname: "Veronica By The Beach Calangute",
        rating: "8.6",
        price: "1400",
        neargooglelink: "https://maps.app.goo.gl/NTK5nvCuNjkiNio7A"
    }

    ]
    }]

export default data;
