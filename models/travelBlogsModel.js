import mongoose from "mongoose";

const travelblogsSchema = mongoose.Schema(
    {
        user:{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
            require: true,
        },
               
        placename:{
            type:String,
            require: true,
        },
        image:{
            type:String,
            require:true,
        },
        Images:{
            type:Array,
        },
        googlelink:{
            type:String,
            require: true,
        },
        placedes:{
            type:String,
            require: true,
        },
        nearbyplaces:{
            type:Array,
        },
        author:{
            type:String
        },
        hotels:{
            type:Array,
        },
    },
    {
		timestamps: true,
	}

);

const TravelBlog = mongoose.model('TravelBlog', travelblogsSchema);


export default TravelBlog;