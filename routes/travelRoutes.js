import express from 'express';
import { createTravelBlog, deletetravelblog, getTravelBlogId, getTravelBlogs, updateTravelBlog } from '../controllers/travelblogsController.js';
import {protect} from '../middlewares/authmiddleware.js';
const router = express.Router();

router.route('/').get(getTravelBlogs).post(protect, createTravelBlog);
router.route('/:id').get(getTravelBlogId).delete(protect, deletetravelblog).put(protect, updateTravelBlog)

export default router;