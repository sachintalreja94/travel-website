import colors from 'colors';
import dotenv from 'dotenv';
import connectDB from './config/db.js';
import data from './info/data.js';
import users from './info/users.js';
import User from './models/userModel.js';
import TravelBlog from './models/travelBlogsModel.js';


dotenv.config();

connectDB();

const importData = async () =>{
    try{
        await User.deleteMany();
        await TravelBlog.deleteMany();

        const createdUsers = await User.insertMany(users);
        const adminUser = createdUsers[0]._id;

        const sampleProducts = data.map((val)=>{
            return {...val, user:adminUser};
        })

        await TravelBlog.insertMany(sampleProducts);

        console.log('Data imported'.green.inverse);
        process.exit();
    } catch(err){
        console.error(`${err}`.red.inverse);
        process.exit(1);
    }
};

const destroyData = async () =>{
    try{
        await TravelBlog.deleteMany();
        await User.deleteMany();
        
        console.log('Data Destroyed'.red.inverse);
        process.exit();
    } catch(err){
        console.error(`${err}`.red.inverse);
        process.exit(1)
    }
}

if(process.argv[2] === '-d'){
    destroyData();

} else{
    importData();
}