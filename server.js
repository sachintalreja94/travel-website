import express from 'express';
import data from './info/data.js';
import dotenv from 'dotenv';
import connectDB from './config/db.js';
import colors from 'colors';
import travelRoutes from './routes/travelRoutes.js';
import { errorHandler } from './middlewares/errorMiddleware.js';
import userRoutes from './routes/userRoutes.js';
import uploadRoutes from './routes/uploadRoutes.js';
import path from 'path';
dotenv.config();

connectDB();

const app = express();

app.use(express.json()); // Request Body Parsing

app.get('/', (req, res) => {
    res.send('API is running...');
});


app.use('/api/travelblogs', travelRoutes);
app.use('/api/users', userRoutes);
app.use('/api/uploads', uploadRoutes);

const __dirname = path.resolve();
app.use('/uploads', express.static(path.join(__dirname, '/uploads')));

app.use(errorHandler);


const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
    console.log(`Server running on ${process.env.NODE_ENV} mode on port ${process.env.PORT}`.yellow.bold);
});