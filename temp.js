import React, { useState } from 'react';
import { Box, Button, Flex, Heading, Image, Link } from '@chakra-ui/react';
import { Link as RouterLink } from 'react-router-dom';
import { FaLocationDot } from "react-icons/fa6";
import { FaChevronLeft, FaChevronRight } from "react-icons/fa";

const TravelCard = ({ travelcard }) => {
    // const [currentImageIndex, setCurrentImageIndex] = useState(0);

    // const nextImage = () => {
    //     setCurrentImageIndex((prevIndex) => (prevIndex + 1) % travelcard.Images.length);
    // };

    // const prevImage = () => {
    //     setCurrentImageIndex((prevIndex) => (prevIndex - 1 + travelcard.Images.length) % travelcard.Images.length);
    // };

    // if (!travelcard || !travelcard.Images) {
    //     return null; // or return a placeholder component
    // }

    return (
        <>
        {/*Images[currentImageIndex]*/}
            <Box borderRadius='lg' bgColor='white' _hover={{ shadow: 'md' }}>
                <Link as={RouterLink} to={`/travelblogs/${travelcard._id}`}>
                    <Image
                        src={travelcard?.image}
                        alt={travelcard.placename}
                        w='full'
                        h='250px'
                        objectFit='cover'
                        borderRadius={{ base: '10px', md: '10px' }}
                    />

                </Link>
                {/* {travelcard.Images.length > 1 && (
                    <Flex justifyContent='center' mt='2'>
                        <Button variant='transparent' onClick={prevImage}><FaChevronLeft /></Button>
                        <Button variant='transparent' onClick={nextImage}><FaChevronRight /></Button>
                    </Flex>
                )} */}
                <Flex py='5' px='4' direction='column' justifyContent='space-between'>
                    <Heading as='h4' fontSize='md' mb='3'>
                        {travelcard.placename}
                    </Heading>
                    <Link as={RouterLink} to={travelcard.googlelink} target='_blank'>
                        <FaLocationDot />
                    </Link>
                </Flex>
                
            </Box>

        </>
    );
};

export default TravelCard;
