import { Routes, Route } from 'react-router-dom';
import HomeScreen from '../src/screens/HomeScreen';
import TravelDetailsScreen from './screens/TravelDetailsScreen';
import Header from './components/Header';
import LoginScreen from './screens/LoginScreen';
import RegisterScreen from './screens/RegisterScreen';
import ProfileScreen from './screens/ProfileScreen';
import Blog from './screens/Blog';
import TravelBlogUpdateScreen from './screens/TravelBlogUpdateScreen';
import TravelGuideScreen from './screens/TravelGuideScreen';
function App() {
  return (

    <>
      <Header />

      <Routes>
        <Route path="/" element={<HomeScreen />} />
        <Route path="/travelblogs/:id" element={<TravelDetailsScreen />} />
        <Route path="/login" element={<LoginScreen />} />
        <Route path="/register" element={<RegisterScreen />} />
        <Route path="/profile" element={<ProfileScreen />} />
        <Route path="/myblog" element={<Blog />} />
				<Route path='/user/:id/edit' element={<TravelBlogUpdateScreen />} />
        <Route path="/travelguide" element={<TravelGuideScreen />} />
      </Routes>

    </>

  );
}

export default App;
