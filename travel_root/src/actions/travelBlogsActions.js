import axios from "axios";
import { TRAVEL_BLOGS_REQUEST, TRAVEL_BLOGS_SUCCESS, TRAVEL_BLOGS_FAIL, TRAVEL_DETAILS_REQUEST, TRAVEL_DETAILS_SUCCESS, TRAVEL_DETAILS_FAIL, TRAVEL_BLOG_DELETE_REQUEST, TRAVEL_BLOG_DELETE_SUCCESS, TRAVEL_BLOG_DELETE_FAIL, TRAVEL_BLOG_CREATE_REQUEST, TRAVEL_BLOG_CREATE_SUCCESS, TRAVEL_BLOG_CREATE_FAIL, TRAVEL_BLOG_UPDATE_REQUEST, TRAVEL_BLOG_UPDATE_SUCCESS, TRAVEL_BLOG_UPDATE_FAIL } from '../constants/travelblogsContstants';

export const listTravelBlogs = () => async (dispatch) => {
    try {
        dispatch({ type: TRAVEL_BLOGS_REQUEST });

        const { data } = await axios.get('/api/travelblogs');

        dispatch({ type: TRAVEL_BLOGS_SUCCESS, payload: data });

    } catch (err) {
        dispatch({
            type: TRAVEL_BLOGS_FAIL, payload:
                err.response && err.response.data.message
                    ? err.response.data.message
                    : err.message,
        })
    }
}

export const listTravelDetails = (id) => async (dispatch) => {
	try {
		dispatch({ type: TRAVEL_DETAILS_REQUEST });

		const { data } = await axios.get(`/api/travelblogs/${id}`);

		dispatch({ type: TRAVEL_DETAILS_SUCCESS, payload: data });
	} catch (err) {
		dispatch({
			type: TRAVEL_DETAILS_FAIL,
			payload:
				err.response && err.response.data.message
					? err.response.data.message
					: err.message,
		});
	}
};


export const deletetravelblog = (id) => async (dispatch, getState) => {
	try {
		dispatch({ type: TRAVEL_BLOG_DELETE_REQUEST });

		const {
			userLogin: { userInfo },
		} = getState();

		const config = {
			headers: {
				Authorization: `Bearer ${userInfo.token}`,
			},
		};

		await axios.delete(`/api/travelblogs/${id}`, config);

		dispatch({ type: TRAVEL_BLOG_DELETE_SUCCESS });
	} catch (err) {
		dispatch({
			type: TRAVEL_BLOG_DELETE_FAIL,
			payload:
				err.response && err.response.data.message
					? err.response.data.message
					: err.message,
		});
	}
};

export const createtravelblog = () => async (dispatch, getState) => {
	try {
		dispatch({ type: TRAVEL_BLOG_CREATE_REQUEST });

		const {
			userLogin: { userInfo },
		} = getState();

		const config = {
			headers: {
				Authorization: `Bearer ${userInfo.token}`,
				'Content-Type': 'application/json',
			},
		};

		const { data } = await axios.post(`/api/travelblogs`, {}, config);

		dispatch({ type: TRAVEL_BLOG_CREATE_SUCCESS, payload: data });
	} catch (err) {
		dispatch({
			type: TRAVEL_BLOG_CREATE_FAIL,
			payload:
				err.response && err.response.data.message
					? err.response.data.message
					: err.message,
		});
	}
};

export const updatetravelblog = (travelblog) => async (dispatch, getState) => {
	try {
		dispatch({ type: TRAVEL_BLOG_UPDATE_REQUEST });

		const {
			userLogin: { userInfo },
		} = getState();

		const config = {
			headers: {
				Authorization: `Bearer ${userInfo.token}`,
				'Content-Type': 'application/json',
			},
		};

		const { data } = await axios.put(
			`/api/travelblogs/${travelblog._id}`,
			travelblog,
			config
		);

		dispatch({ type: TRAVEL_BLOG_UPDATE_SUCCESS, payload: data });
	} catch (err) {
		dispatch({
			type: TRAVEL_BLOG_UPDATE_FAIL,
			payload:
				err.response && err.response.data.message
					? err.response.data.message
					: err.message,
		});
	}
};