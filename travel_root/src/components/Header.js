import React from 'react'
import { Button, Flex, Heading, Input, Link, Menu, MenuButton, MenuItem, MenuList } from '@chakra-ui/react';
import { useDispatch, useSelector } from 'react-redux'
import { Link as RouterLink, useNavigate } from 'react-router-dom'
import { logout } from '../actions/userActions';
import { IoChevronDown } from 'react-icons/io5';
import { FiSearch } from "react-icons/fi";
const Header = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const userLogin = useSelector((state) => state.userLogin);
    const { userInfo } = userLogin;

    const logoutHandler = () => {
        dispatch(logout());
        navigate('/login');
    };


    return (
        <>
            {userInfo ? (
                <>
                    <Flex direction={{base:'column', md:'row'}} alignItems="center" justifyContent={{base:'left',md:"space-between"}} mt="1rem" mr="1rem">
                        <Link href='/' _hover={{ textDecor: "none" }}>
                            <Heading as="h3" ml="0.8rem" size='xl'
                                letterSpacing='wide'
                                mb={{base:'1rem', md:'0'}}
                            >TravelGuide</Heading>
                        </Link>

                        <form>
                            <Flex>
                                <Input type='text' placeholder='Source' w={{base:'100px',md:'500px'}} border='1px solid black' _hover={{ border: '1px solid black' }}></Input>
                                <Input type='text' placeholder='Destination' w={{base:'100px',md:'500px'}} border='1px solid black' _hover={{ border: '1px solid black' }} ml='1rem' mr='1rem'></Input>
                                <Link as={RouterLink} to='/travelguide'><Button bgColor='gray.700' _hover={{ bgColor: '#718096', color: 'white' }}><FiSearch color='white' /></Button></Link>
                            </Flex>

                        </form>

                        <Menu>
                            <MenuButton
                            mt={{base:'1rem', md:'0'}}
                                as={Button}
                                rightIcon={<IoChevronDown />}
                                _hover={{ textDecor: 'none', opacity: '0.7', color: 'white', bgColor: '#00B5D8' }}
                                bgColor='#0BC5EA' color='white'>
                                {userInfo.name}
                            </MenuButton>
                            <MenuList>
                                <MenuItem as={RouterLink} to='/profile'>
                                    Profile
                                </MenuItem>
                                <MenuItem as={RouterLink} to='/myblog'>
                                    My Blog
                                </MenuItem>
                                <MenuItem onClick={logoutHandler}>Logout</MenuItem>

                            </MenuList>
                        </Menu>
                    </Flex>


                </>

            ) : (
                <Flex alignItems="center" justifyContent="space-between" mt="1rem" mr="1rem">
                    <Link href='/' _hover={{ textDecor: "none" }}>
                        <Heading as="h3" ml="0.8rem" size='xl'
                            letterSpacing='wide'

                        >TravelGuide</Heading>
                    </Link>

                    <Link as={RouterLink} to='/login'><Button bgColor='#0BC5EA' color='white' _hover={{ bgColor: '#00B5D8', color: 'white' }}>Login</Button></Link>
                </Flex>
            )}
        </>
    )
}

export default Header