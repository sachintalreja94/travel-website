import { Box, Flex, Heading, Image, Link } from '@chakra-ui/react';
import { Link as RouterLink } from 'react-router-dom';
import { FaLocationDot } from "react-icons/fa6";

const TravelCard = ({ travelcard }) => {

    return (
        <>
            <Box borderRadius='lg' bgColor='white' _hover={{ shadow: 'md' }}>
                <Link as={RouterLink} to={`/travelblogs/${travelcard._id}`}>
                    <Image
                        src={travelcard?.image}
                        alt={travelcard.placename}
                        w='full'
                        h='250px'
                        objectFit='cover'
                        borderRadius={{ base: '10px', md: '10px' }}
                    />

                </Link>

                <Flex py='5' px='4' direction='column' justifyContent='space-between'>
                    <Heading as='h4' fontSize='md' mb='3'>
                        {travelcard.placename}
                    </Heading>
                    <Link as={RouterLink} to={travelcard.googlelink} target='_blank'>
                        <FaLocationDot />
                    </Link>
                </Flex>
                
            </Box>

        </>
    );
};

export default TravelCard;
