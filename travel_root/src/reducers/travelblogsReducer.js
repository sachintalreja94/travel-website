import { TRAVEL_BLOGS_REQUEST, TRAVEL_BLOGS_SUCCESS, TRAVEL_BLOGS_FAIL, TRAVEL_DETAILS_REQUEST, TRAVEL_DETAILS_SUCCESS, TRAVEL_DETAILS_FAIL, TRAVEL_BLOG_DELETE_REQUEST, TRAVEL_BLOG_DELETE_SUCCESS, TRAVEL_BLOG_DELETE_FAIL, TRAVEL_BLOG_CREATE_REQUEST, TRAVEL_BLOG_CREATE_SUCCESS, TRAVEL_BLOG_CREATE_FAIL, TRAVEL_BLOG_CREATE_RESET, TRAVEL_BLOG_UPDATE_REQUEST, TRAVEL_BLOG_UPDATE_SUCCESS, TRAVEL_BLOG_UPDATE_FAIL, TRAVEL_BLOG_UPDATE_RESET } from '../constants/travelblogsContstants';

export const travelblogslistReducer = (state = { travelblogs: [] }, action) => {
    switch (action.type) {
        case TRAVEL_BLOGS_REQUEST:
            return { loading: true, travelblogs: [] };
        case TRAVEL_BLOGS_SUCCESS:
            return { loading: false, travelblogs: action.payload };
        case TRAVEL_BLOGS_FAIL:
            return { loading: false, error: action.payload };
        default:
            return state;
    }
}

export const travelblogDetailsReducer = (state = { travelblog: {} }, action) => {
    switch (action.type) {
        case TRAVEL_DETAILS_REQUEST:
            return { ...state, loading: true };
        case TRAVEL_DETAILS_SUCCESS:
            return { loading: false, travelblog: action.payload };
        case TRAVEL_DETAILS_FAIL:
            return { loading: false, error: action.payload };
        default:
            return state;
    }
};

export const travelblogDeleteReducer = (state = {}, action) => {
    switch (action.type) {
        case TRAVEL_BLOG_DELETE_REQUEST:
            return { ...state, loading: true };
        case TRAVEL_BLOG_DELETE_SUCCESS:
            return { loading: false, success: true };
        case TRAVEL_BLOG_DELETE_FAIL:
            return { loading: false, error: action.payload };
        default:
            return state;
    }
};

export const travelblogCreateReducer = (state = {}, action) => {
    switch (action.type) {
        case TRAVEL_BLOG_CREATE_REQUEST:
            return { ...state, loading: true };;
        case TRAVEL_BLOG_CREATE_SUCCESS:
            return { loading: false, travelblog: action.payload, success: true };
        case TRAVEL_BLOG_CREATE_FAIL:
            return { loading: false, error: action.payload };
        case TRAVEL_BLOG_CREATE_RESET:
            return {};
        default:
            return state;
    }
}

export const travelblogUpdateReducer = (state ={travelblog:{}}, action) => {
    switch(action.type){
    case TRAVEL_BLOG_UPDATE_REQUEST:
        return {...state, loading:true};;
    case TRAVEL_BLOG_UPDATE_SUCCESS:
        return {loading:false, travelblog: action.payload, success:true};
    case TRAVEL_BLOG_UPDATE_FAIL:
        return {loading:false, error: action.payload};
    case TRAVEL_BLOG_UPDATE_RESET:
        return {travelblog:{}};
    default:
        return state;
}
}