import React from 'react';
import { Flex, Grid, Heading, Link, Box, Image, Button, Icon } from '@chakra-ui/react';
import { createtravelblog, deletetravelblog, listTravelBlogs } from '../actions/travelBlogsActions';
import { useDispatch, useSelector } from 'react-redux';
import { useEffect } from 'react';
import Loader from '../components/Loader';
import Message from '../components/Message';
import { FaLocationDot } from "react-icons/fa6";
import { Link as RouterLink, useNavigate } from 'react-router-dom';
import { IoAdd, IoPencilSharp, IoTrashBinSharp } from 'react-icons/io5';
import { TRAVEL_BLOG_CREATE_RESET } from '../constants/travelblogsContstants';

const Blog = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const travelblogslist = useSelector((state) => state.travelblogslist);
    const { loading, error, travelblogs } = travelblogslist;

    const userLogin = useSelector((state) => state.userLogin);
    const { userInfo } = userLogin;

    const traveblogCreate = useSelector((state) => state.traveblogCreate);
    const {
        loading: loadingCreate,
        error: errorCreate,
        success: successCreate,
        travelblog: createdtravelblog,
    } = traveblogCreate;

    const traveblogDelete = useSelector((state) => state.traveblogDelete);
    const {
        loading: loadingDelete,
        error: errorDelete,
        success: successDelete,
    } = traveblogDelete;

    useEffect(() => {
        dispatch({ type: TRAVEL_BLOG_CREATE_RESET });

        if (!userInfo) {
            navigate('/login')
        } else if (successCreate) {
            navigate(`/user/${createdtravelblog._id}/edit`);
        }
        else {

            dispatch(listTravelBlogs());
        }
    }, [dispatch,navigate, userInfo, successDelete, successCreate, createdtravelblog]);

    const deleteHandler = (id) => {
        if (window.confirm('Are you sure?')) {
            dispatch(deletetravelblog(id));
        }
        // alert("deleteblog")
    };

    const createProductHandler = () => {
        dispatch(createtravelblog());
        // alert('Create Product')
    }

    const userTravelBlogs = travelblogs.filter(tblog => {
		const blogAuthor = tblog?.author && tblog?.author.toLowerCase().trim();
		const userName = userInfo.name && userInfo.name.toLowerCase().trim();
		console.log(`Comparing blog author: ${blogAuthor} with user name: ${userName}`);
		return blogAuthor === userName;
	});

    return (
        < >
            <Flex alignItems='center' justifyContent='space-between'>
                <Heading as='h2' mb='8' mt={{ base: '1rem', md: '1rem' }} fontSize='xl' textAlign={{ base: 'center', md: 'left' }} ml={{ base: '1rem', md: '1rem' }}>
                    My Blogs
                </Heading>

                <Button onClick={createProductHandler} colorScheme='teal' w={{ base: 'fit-content', md: 'auto' }} mr={{ base: '1rem', md: '1rem' }}>
                    <Icon as={IoAdd} mr='2' fontSize={'xl'} fontWeight={'bold'}>
                    </Icon>
                    Create Post
                </Button>

            </Flex>

            {loadingCreate && <Loader />}
            {errorCreate && <Message type='error'>{errorCreate}</Message>}

            {loadingDelete && <Loader />}
            {errorDelete && <Message type='error'>{errorDelete}</Message>}

            {
                loading ? (
                    <Loader></Loader>
                ) : error ? (
                    <Message type='error'>{error}</Message>
                ) : (
                    <Grid templateColumns={{ base: '1fr', md: '1fr' }} gap='8' ml={{ base: '1rem', md: '1rem' }} mr={{ base: '1rem', md: '1rem' }}>
                        {console.log(travelblogs)}
                        {userTravelBlogs?.map((val) => (
                            
                            <Box key={val._id} width={{ base: 'fit-content', md: '800px' }} mb='1rem' mt='1rem' mr={{ base: '0rem', md: 'auto' }} ml={{ base: '0rem', md: 'auto' }}>
                                <Flex gap='2rem' border='2px solid gray' boxShadow='md' borderRadius='md' alignItems={{ base: 'left', md: 'center' }} pb='1rem' direction={{ base: 'column', md: 'row' }}>
                                    <Box borderRadius='lg' bgColor='white' _hover={{ shadow: 'md' }} mb='1rem' mt='1rem' boxShadow='md' ml={{ base: '0', md: '1rem' }}>
                                        <Image
                                            src={val?.image}
                                            alt={val.placename}
                                            w={{ base: 'auto', md: '300px' }}
                                            h={{ base: '250px', md: '130px' }}
                                            borderRadius={{ base: '0', md: 'md' }}
                                            objectFit='cover'
                                        />

                                    </Box>
                                    <Box>
                                        <Heading as='h4' fontSize='md' mb='3'>
                                            {val.placename}
                                        </Heading>

                                        <Link as={RouterLink} to={val.googlelink} target='_blank' >
                                            <FaLocationDot />
                                        </Link>


                                        <Box>
                                            <Flex alignItems={{ base: 'center', md: 'left' }} justifyContent={{ base: 'center', md: 'left' }}>
                                                <Button
                                                    mt='1rem'
                                                    mr='4'
                                                    colorScheme='red'
                                                    onClick={() => deleteHandler(val._id)}>
                                                    <Icon as={IoTrashBinSharp} color='white' size='sm' />
                                                </Button>
                                                <Button mt='1rem' mr='4' as={RouterLink} to={`/user/${val._id}/edit`} colorScheme='teal'>
                                                    <Icon as={IoPencilSharp} color={'white'} size={'sm'}></Icon>
                                                </Button>
                                            </Flex>
                                        </Box>
                                    </Box>

                                </Flex>
                            </Box>


                        ))}
                    </Grid >
                )
            }






        </>
    )
}

export default Blog
