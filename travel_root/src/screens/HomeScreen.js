import { Flex, Grid, Heading } from '@chakra-ui/react';
import TravelCard from '../components/TravelCard';
import { useEffect } from 'react';
import { listTravelBlogs } from '../actions/travelBlogsActions';
import {useDispatch, useSelector} from 'react-redux'
import Loader from '../components/Loader';
import Message from '../components/Message';
const HomeScreen = () => {
	const dispatch = useDispatch();
	const travelblogslist = useSelector((state) => state.travelblogslist);
	const { loading, error, travelblogs } = travelblogslist;

	useEffect(() => {
		dispatch(listTravelBlogs());
	}, [dispatch]);

	return (
		<>
			<Flex alignItems='center' justifyContent='space-between'>
				<Heading as='h2' mb='8' mt={{ base: '1rem', md: '1rem' }} fontSize='xl' textAlign={{ base: 'center', md: 'left' }} ml={{ base: '1rem', md: '1rem' }}>
					Places To Visit
				</Heading>

			</Flex>

			{
				loading ? (
					<Loader></Loader>
				) : error ? (
					<Message type='error'>{error}</Message>
				) : (
					<Grid templateColumns={{ base: '1fr', md: '1fr 1fr 1fr 1fr' }} gap='8' ml={{ base: '1rem', md: '1rem' }} mr={{ base: '1rem', md: '1rem' }}>
						{travelblogs?.map((val) => (
							<TravelCard key={val._id} travelcard={val} />
						))}
					</Grid >
				)
			}



		</>
	);
};

export default HomeScreen;