import { Flex, Heading, FormControl, FormLabel, Spacer, Input, Button, Textarea } from '@chakra-ui/react';
import React, { useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';
import { TRAVEL_BLOG_UPDATE_RESET } from '../constants/travelblogsContstants';
import { listTravelDetails } from '../actions/travelBlogsActions';
import Loader from '../components/Loader';
import Message from '../components/Message';
import axios from 'axios';
import { updatetravelblog } from '../actions/travelBlogsActions';
import FormContainer from '../components/FormContainer';
const TravelBlogUpdateScreen = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { id: travelblogId } = useParams();

  const [placename, setPlacename] = useState('');
  const [image, setImage] = useState('');
  const [googlelink, setGoogleink] = useState('');
  const [placedes, setPlacedes] = useState('');
  // const [name, setName] = useState('');
  const [nearbyplaces, setNearbyplaces] = useState('');

  // const userLogin = useSelector((state) => state.userLogin);
  // const { userInfo } = userLogin;

  const traveblogDetails = useSelector((state) => state.traveblogDetails);
  const { loading, error, travelblog } = traveblogDetails;

  const travelblogUpdate = useSelector((state) => state.travelblogUpdate);
  const { loading: loadingUpdate, error: errorUpdate, success: successUpdate } = travelblogUpdate;

  useEffect(() => {
    if (successUpdate) {
      dispatch({ type: TRAVEL_BLOG_UPDATE_RESET })
      navigate('/myblog');
    } else if (!travelblog.placename || travelblog._id !== travelblogId) {
      dispatch(listTravelDetails(travelblogId))
    } else {
      setPlacename(travelblog.placename);
      setPlacedes(travelblog.placedes);
      setImage(travelblog.image);
      // setName(travelblog.name);
      setGoogleink(travelblog.googlelink);
      setNearbyplaces(travelblog.nearbyplaces);
    }

  }, [dispatch, navigate, travelblogId, travelblog, successUpdate]);

  const submitHandler = (e) => {
    e.preventDefault();

    dispatch(
      updatetravelblog({
        _id: travelblogId,
        // name: userInfo.name,
        placename,
        image,
        googlelink,
        placedes,
        nearbyplaces,
      })
    )
  }

  const uploadFileHandler = async (e) => {
    const file = e.target.files[0];
    const formData = new FormData();
    formData.append('image', file);

    try {
      const config = {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      };
      const { data } = await axios.post(`/api/uploads`, formData, config);
      setImage(data);
    } catch (err) {
      console.error(err);
    }
  }
  return (
    <>
      <Button onClick={() => { navigate(-1) }} colorScheme='gray' mb='5' mt={{ base: '1rem', md: '1rem' }} ml={{ base: '1rem', md: '1rem' }}>Go Back</Button>
      <Flex w='full' alignItems={'center'} justifyContent={'center'} py='5'>
        <FormContainer>
          <Heading as='h1' mb='8' fontSize='3xl'>Create Post</Heading>
          {loadingUpdate && <Loader />}
          {errorUpdate && <Message type='error'>{errorUpdate}</Message>}

          {
            loading ? (
              <Loader></Loader>
            ) : error ? (
              <Message type='error'>{error}</Message>
            ) : (
              <form onSubmit={submitHandler}>
                {/* Name */}
                {/* <FormControl id='Name' isRequired>
                  <FormLabel>Name</FormLabel>
                  <Input type='text' placeholder='Enter Name' value={name} onChange={(e) => { setName(e.target.value) }}></Input>
                </FormControl>
                <Spacer h='3'></Spacer> */}

                {/* Place Name */}
                <FormControl id='placeName' isRequired>
                  <FormLabel>Place Name</FormLabel>
                  <Input type='text' placeholder='Enter Place Name' value={placename} onChange={(e) => { setPlacename(e.target.value) }} border='1px solid black'></Input>
                </FormControl>

                {/* Image */}

                <FormControl id='image' isRequired>
                  <FormLabel>Image</FormLabel>
                  <Input
                    type='text'
                    placeholder='Enter image url'
                    value={image}
                    onChange={(e) => setImage(e.target.value)}
                  />
                  <Input type='file' onChange={uploadFileHandler} />
                </FormControl>
                <Spacer h='3' />
                {/* GoogleLink */}

                <FormControl id='googleLink' isRequired>
                  <FormLabel>Location</FormLabel>
                  <Input type='text' placeholder='Enter google map link' value={googlelink} onChange={(e) => { setGoogleink(e.target.value) }} border='1px solid black'></Input>
                </FormControl>
                <Spacer h='3'></Spacer>

                <Spacer h='3'></Spacer>
                {/* Place Description */}
                <FormControl id='placeDesc'>
                  <FormLabel>Place Description</FormLabel>
                  <Textarea rows='10' cols='10' value={placedes} onChange={(e) => { setPlacedes(e.target.value) }} border='1px solid black'></Textarea>
                </FormControl>
                <Spacer h='3'></Spacer>

                {/* Nearby Places */}

                <FormControl>
                  <FormLabel>Nearby Places</FormLabel>
                  <Input type='text' placeholder='Enter Nearby Places' value={nearbyplaces} onChange={(e) => { setNearbyplaces(e.target.value) }} border='1px solid black'></Input>
                </FormControl>
                <Spacer h='3'></Spacer>

                <Button type='submit' isLoading={loading} colorScheme='teal' mt='4'>Create</Button>
              </form>
            )}
        </FormContainer>
      </Flex>

    </>
  )
}

export default TravelBlogUpdateScreen