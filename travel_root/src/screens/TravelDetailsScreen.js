import { Button, Flex, Grid, Heading, Image, Text, Link, Box } from '@chakra-ui/react'
import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom';
import { Link as RouterLink } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { listTravelDetails } from '../actions/travelBlogsActions';
import Loader from '../components/Loader';
import Message from '../components/Message';
import { FaChevronLeft, FaChevronRight } from "react-icons/fa";
const TravelDetailsScreen = () => {
    const { id } = useParams();
    const dispatch = useDispatch();

    const traveblogDetails = useSelector((state) => state.traveblogDetails);
    const { loading, error, travelblog } = traveblogDetails;

    useEffect(() => {
        dispatch(listTravelDetails(id));
    }, [id, dispatch]);

    const [currentImageIndex, setCurrentImageIndex] = useState(0);
    const nextImage = () => {
        setCurrentImageIndex((prevIndex) => (prevIndex + 1) % travelblog.Images.length);
    };

    const prevImage = () => {
        setCurrentImageIndex((prevIndex) => (prevIndex - 1 + travelblog.Images.length) % travelblog.Images.length);
    };

    if (!travelblog || !travelblog.Images) {
        return null; // or return a placeholder component
    }
    return (
        <>
            <Flex mb='5' mt={{ base: '1rem', md: '1rem' }} ml={{ base: '1rem', md: '1rem' }}>
                <Button as={RouterLink} to='/' colorScheme='gray'>
                    Go Back
                </Button>
            </Flex>

            {loading ? (
                <Loader />
            ) : error ? (
                <Message type='error'>{error}</Message>
            ) : (


                <Grid templateColumns={{ base: '1fr', md: '5fr 4fr' }} gap='10' ml={{ base: '1rem', md: '1rem' }} mr={{ base: '1rem', md: '1rem' }}>
                    {/* Column 1 */}
                    <Box>
                        <Image src={travelblog.Images[currentImageIndex]} w='100%' h='500px' alt={travelblog.placename} borderRadius={{base:'none',md:'md'}} />
                        {travelblog.Images.length > 1 && (
                            <Flex justifyContent='center' mt='2'>
                                <Button variant='transparent' onClick={prevImage}><FaChevronLeft /></Button>
                                <Button variant='transparent' onClick={nextImage}><FaChevronRight /></Button>
                            </Flex>
                        )}
                    </Box>

                    {/* Column 2 */}
                    <Flex direction='column'>
                        {/* <Heading as='h5' fontSize='base' color='gray.500'>
                        {travelblog.brand}
                    </Heading> */}

                        <Heading as='h2' fontSize='4xl' mb='4'>
                            {travelblog.placename}
                        </Heading>


                        <Text mb='1rem'>{travelblog.placedes}</Text>


                        <Link as={RouterLink} to={travelblog.googlelink} target='_blank' mb='1rem'><Button bgColor='red.500' textColor='white' _hover={{ backgroundColor: 'red' }}><Text fontSize='xl' fontWeight='500'>Google Location</Text></Button></Link>

                        <Text fontSize='xl' fontWeight='bold'> Nearby Places</Text>

                        {
                            travelblog && (<>
                                <Text>{travelblog.nearbyplaces}</Text>
                                <Text fontSize='xl' fontWeight='bold' mt='1rem'>Nearby Hotels</Text>

                                {

                                    travelblog?.hotels?.map((hotel, index) => (
                                        <Box key={index} borderWidth='1px' borderRadius='lg' p='4' mt='2' mb='2'>
                                            <Heading as='h3' fontSize='2xl'>{hotel.nearbyname}</Heading>
                                            <Text>Rating: {hotel.rating}</Text>
                                            <Text>Price: {hotel.price}</Text>
                                            <Link as={RouterLink} to={hotel.neargooglelink} target='_blank'>
                                                View on Google Maps</Link>
                                        </Box>
                                    ))
                                }
                            </>
                            )
                        }

                    </Flex>

                </Grid>
            )}
        </>
    )
}

export default TravelDetailsScreen