import {  Flex, Heading, Image, Link, Table, TableContainer, Tbody, Td, Text, Th, Thead, Tr } from '@chakra-ui/react'
import React from 'react'
import map from '../images/mumbai-to-pune.png';
import { FaCar } from "react-icons/fa6";
import { RiMotorbikeFill } from "react-icons/ri";
import { FaTrainSubway } from "react-icons/fa6";
import { FaWalking } from "react-icons/fa";
import { FaBusSimple } from "react-icons/fa6";

const TravelGuideScreen = () => {
    return (
        <>
            <Flex alignItems='center' justifyContent='space-around' mt={{base:'2rem', md:'1rem'}}>
                <Flex alignItems='center' justifyContent='center' direction='column'>
                    <FaCar />
                    <Text>3hr 4mins</Text>
                </Flex>

                <Flex alignItems='center' justifyContent='center' direction='column'>
                    <RiMotorbikeFill />
                    <Text>3hr 35mins</Text>
                </Flex>
                <Flex alignItems='center' justifyContent='center' direction='column'>
                    <Link href='#train-details'><FaTrainSubway /></Link>
                    <Text>3hr 25mins</Text>
                </Flex>
                <Flex alignItems='center' justifyContent='center' direction='column'>
                    <Link href='#bus-details'><FaBusSimple /></Link>
                    <Text>4hr 05mins</Text>

                </Flex>
                <Flex alignItems='center' justifyContent='center' direction='column'>
                    <FaWalking />
                    <Text>2 days</Text>
                </Flex>
            </Flex>

            <Flex alignItems='center' justifyContent='center' mt='2rem'>

                <Image src={map} w={{base:'100%',md:'80%'}} h='500px' borderRadius={{baase:'0',md:'30px'}}></Image>
            </Flex>

            <Heading as='h3' textAlign='center' mt='2rem' mb='1rem'>List of Train Details</Heading>

            <TableContainer id='train-details'>
                <Table variant='striped'>
                    <Thead>
                        <Tr>
                            <Th>Name</Th>
                            <Th>Time</Th>
                            <Th>Duration</Th>
                        </Tr>
                    </Thead>
                    <Tbody>
                        <Tr>
                            <Td>Pragati Express</Td>
                            <Td>4:43PM - 7:57PM</Td>
                            <Td>3hrs 14mins</Td>
                        </Tr>
                        <Tr>
                            <Td>Decaan Queen</Td>
                            <Td>5:10PM - 8:25PM</Td>
                            <Td>3hrs 15mins</Td>
                        </Tr>
                        <Tr>
                            <Td>M.G.R Chennai Central Weekly SF Express</Td>
                            <Td>5:42PM - 9:10PM</Td>
                            <Td>3hrs 28mins</Td>
                        </Tr>
                    </Tbody>
                </Table>
            </TableContainer>

            <Heading as='h3' textAlign='center' mt='2rem' mb='1rem'>List of Bus Details</Heading>

            <TableContainer id='bus-details'>
                <Table variant='striped'>
                    <Thead>
                        <Tr>
                            <Th>Name</Th>
                            <Th>Time</Th>
                            <Th>Duration</Th>
                            <Th>Price</Th>
                        </Tr>
                    </Thead>
                    <Tbody>
                        <Tr>
                            <Td>Pinkbus</Td>
                            <Td>4:16PM - 9:00PM</Td>
                            <Td>4h 05mins</Td>
                            <Td>₹355</Td>
                        </Tr>
                        <Tr>
                            <Td>N.T Abhinav</Td>
                            <Td>4:16PM - 9:00PM</Td>
                            <Td>4h 05mins</Td>
                            <Td>₹529</Td>
                        </Tr>
                        <Tr>
                            <Td>N.T Yashshree</Td>
                            <Td>4:16PM - 10:00PM</Td>
                            <Td>5hrs 05mins</Td>
                            <Td>₹567</Td>
                        </Tr>
                    </Tbody>
                </Table>
            </TableContainer>

        </>
    )
}

export default TravelGuideScreen