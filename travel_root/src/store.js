import { applyMiddleware, combineReducers, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import { userDetailsReducer, userLoginReducer, userRegisterReducer, userUpdateProfileReducer } from './reducers/userReducer';
import { travelblogCreateReducer, travelblogDeleteReducer, travelblogDetailsReducer, travelblogslistReducer, travelblogUpdateReducer } from './reducers/travelblogsReducer';

const reducer = combineReducers({
	userLogin: userLoginReducer,
	userRegister: userRegisterReducer,
	userUpdateProfile: userUpdateProfileReducer,
	userDetails: userDetailsReducer,
	travelblogslist: travelblogslistReducer,
	traveblogDetails: travelblogDetailsReducer,
	traveblogCreate: travelblogCreateReducer,
	traveblogDelete: travelblogDeleteReducer,
	travelblogUpdate: travelblogUpdateReducer,
});

const userInfoFromStorage = localStorage.getItem('userInfo')
	? JSON.parse(localStorage.getItem('userInfo'))
	: null;


const initialState = {
	userLogin: { userInfo: userInfoFromStorage },
};

const middlewares = [thunk];

const store = createStore(
	reducer,
	initialState,
	composeWithDevTools(applyMiddleware(...middlewares))
);

export default store;
